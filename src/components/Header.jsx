import { useState, useEffect } from "react";
import Suitmedia from "../assets/suitmedia2.png";

export default function Navbar() {
  const [selectedItem, setSelectedItem] = useState("Ideas"); // Set the initial selected item to "Ideas"
  const [scrollDirection, setScrollDirection] = useState("up");
  const [scrollPosition, setScrollPosition] = useState(0);

  const handleItemClick = (item) => {
    setSelectedItem(item);
  };

  // Function to detect scroll direction
  const handleScroll = () => {
    const currentScrollPosition = window.pageYOffset;
    if (currentScrollPosition < scrollPosition) {
      setScrollDirection("up");
    } else {
      setScrollDirection("down");
    }
    setScrollPosition(currentScrollPosition);
  };

  // Add event listener for scroll
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [scrollPosition]);

  return (
    <div
      className={`py-6 bg-[#FF6600] fixed w-full top-0 z-50 transition-all duration-300 ${
        scrollDirection === "down" ? "-translate-y-full" : "translate-y-0"
      }`}
      style={{ opacity: scrollPosition === 0 ? 1 : 0.8 }}
    >
      <div className="flex items-center justify-between max-w-screen-xl mx-auto">
        <img src={Suitmedia} alt="Suitmedia Logo" className="h-[3rem]" />
        <ul className="flex items-center gap-8 text-xl text-white cursor-pointer">
          <li
            onClick={() => handleItemClick("Work")}
            className={`${
              selectedItem === "Work"
                ? "border-b-4 border-white pb-2"
                : " border-b-4 border-[#FF6600] pb-2"
            }`}
          >
            <span>Work</span>
          </li>
          <li
            onClick={() => handleItemClick("About")}
            className={`${
              selectedItem === "About"
                ? "border-b-4 border-white pb-2"
                : "border-b-4 border-[#FF6600] pb-2"
            }`}
          >
            <span>About</span>
          </li>
          <li
            onClick={() => handleItemClick("Services")}
            className={`${
              selectedItem === "Services"
                ? "border-b-4 border-white pb-2"
                : "border-b-4 border-[#FF6600] pb-2"
            }`}
          >
            <span>Services</span>
          </li>
          <li
            onClick={() => handleItemClick("Ideas")}
            className={`${
              selectedItem === "Ideas"
                ? "border-b-4 border-white pb-2"
                : "border-b-4 border-[#FF6600] pb-2"
            }`}
          >
            <span>Ideas</span>
          </li>
          <li
            onClick={() => handleItemClick("Careers")}
            className={`${
              selectedItem === "Careers"
                ? "border-b-4 border-white pb-2"
                : "border-b-4 border-[#FF6600] pb-2"
            }`}
          >
            <span>Careers</span>
          </li>
          <li
            onClick={() => handleItemClick("Contact")}
            className={`${
              selectedItem === "Contact"
                ? "border-b-4 border-white pb-2"
                : "border-b-4 border-[#FF6600] pb-2"
            }`}
          >
            <span>Contact</span>
          </li>
        </ul>
      </div>
    </div>
  );
}
