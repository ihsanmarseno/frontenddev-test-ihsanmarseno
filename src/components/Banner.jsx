import { useEffect, useState } from "react";

export default function Banner() {
  const [offset, setOffset] = useState(0);

  const handleScroll = () => setOffset(window.pageYOffset);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  return (
    <div className="relative w-full h-full overflow-hidden">
      <div className="relative">
        <img
          src="https://suitmedia.static-assets.id/storage/files/601/6.jpg"
          alt="Banner"
          className="object-cover w-full h-[36rem] bg-blend-darken"
          style={{ transform: `translateY(${offset * 0.5}px)` }}
          loading="lazy"
        />
        <div
          className="absolute inset-0 z-40 overflow-hidden"
          style={{
            clipPath: "polygon(0 100%, 100% 100%, 100% 70%)",
            WebkitClipPath: "polygon(0 100%, 100% 100%, 100% 70%)",
          }}
        >
          <div className="w-full h-full border bg-gray-50 border-gray-50"></div>
        </div>
      </div>
      <div className="absolute inset-0 bg-black opacity-50"></div>
      <div
        className="absolute inset-0 flex flex-col items-center justify-center text-wrapper"
        style={{ transform: `translateY(${offset * 0.8}px)` }}
      >
        <h1 className="text-6xl font-semibold text-white headline">Ideas</h1>
        <h2 className="text-4xl text-white sub-headline">
          Where all our great things begin
        </h2>
      </div>
    </div>
  );
}
