import { useEffect, useState } from "react";
import axios from "axios";
import ReactPaginate from "react-paginate";
import Card from "./Card";

const Hero = () => {
  // Retrieve values from localStorage or use default values
  const initialPage = parseInt(localStorage.getItem("page"), 10) || 0;
  const initialItemsPerPage =
    parseInt(localStorage.getItem("itemsPerPage"), 10) || 10;
  const initialSortBy = localStorage.getItem("sortBy") || "-published_at";

  // State values
  const [ideas, setIdeas] = useState([]);
  const [page, setPage] = useState(initialPage);
  const [itemsPerPage, setItemsPerPage] = useState(initialItemsPerPage);
  const [sortBy, setSortBy] = useState(initialSortBy);
  const sortOptions = [
    { value: "-published_at", label: "Newest" },
    { value: "published_at", label: "Oldest" },
  ];

  useEffect(() => {
    const fetchData = async () => {
      try {
        const apiUrl = "https://suitmedia-backend.suitdev.com/api/ideas";
        const params = {
          "page[number]": page + 1,
          "page[size]": itemsPerPage,
          "append[]": ["small_image", "medium_image"],
          sort: sortBy,
        };

        const response = await axios.get(apiUrl, { params });
        const data = response.data;

        setIdeas(data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, [page, itemsPerPage, sortBy]);

  useEffect(() => {
    localStorage.setItem("page", String(page));
    localStorage.setItem("itemsPerPage", String(itemsPerPage));
    localStorage.setItem("sortBy", sortBy);
  }, [page, itemsPerPage, sortBy]);

  const handlePageChange = (event) => {
    const newPage = event.selected;
    const validPage = Math.min(newPage, Math.max(0, pageCount - 1));
    setPage(validPage);
  };

  const handleSortChange = (event) => {
    setSortBy(event.target.value);
    setPage(0);
  };

  const handleItemsPerPageChange = (event) => {
    setItemsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const pageCount = Math.max(
    1,
    Math.ceil((ideas.meta?.total || 0) / itemsPerPage)
  );
  const showingInfo = `Showing ${Math.min(
    (page + 1) * itemsPerPage - itemsPerPage + 1,
    ideas.meta?.total || 0
  )}-${Math.min((page + 1) * itemsPerPage, ideas.meta?.total || 0)} of ${
    ideas.meta?.total || 0
  }`;

  return (
    <div className="max-w-screen-xl pb-8 mx-auto mt-8">
      <div className="flex items-center justify-between mb-8 font-semibold">
        <div className="flex items-center">
          <p>{showingInfo}</p>
        </div>
        <div className="flex items-center">
          <label className="mr-2">Show per page:</label>
          <select
            value={itemsPerPage}
            onChange={handleItemsPerPageChange}
            className="px-8 py-2 mr-6 border border-gray-400 rounded-full bg-gray-50"
          >
            {[10, 20, 50].map((option) => (
              <option key={option} value={option}>
                {option}
              </option>
            ))}
          </select>
          <label className="mr-2">Sort by:</label>
          <select
            value={sortBy}
            onChange={handleSortChange}
            className="px-8 py-2 mr-2 border border-gray-400 rounded-full bg-gray-50"
          >
            {sortOptions.map((option) => (
              <option key={option.value} value={option.value}>
                {option.label}
              </option>
            ))}
          </select>
        </div>
      </div>

      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-x-6 gap-y-8">
        {ideas.data?.map((idea) => (
          <Card key={idea.id} idea={idea} />
        ))}
      </div>

      {ideas.meta?.total > 0 && (
        <ReactPaginate
          breakLabel="..."
          nextLabel=">"
          onPageChange={handlePageChange}
          pageRangeDisplayed={3}
          pageCount={pageCount}
          initialPage={page}
          previousLabel="<"
          containerClassName="flex items-center justify-center mt-8"
          pageClassName="mx-2"
          pageLinkClassName="text-black px-3 py-2 rounded hover:bg-[#FF6600] z-0 font-semibold"
          activeClassName="px-1 py-2 rounded z-100 bg-[#FF6600]"
          previousClassName={`mx-2 ${
            page === 0 ? "opacity-50 cursor-not-allowed" : ""
          }`}
          nextClassName={`mx-2 ${
            page === pageCount - 1 ? "opacity-50 cursor-not-allowed" : ""
          }`}
          previousLinkClassName={`px-3 py-2 rounded hover:bg-[#FF6600] font-semibold ${
            page === 0 ? "cursor-not-allowed" : ""
          }`}
          nextLinkClassName={`px-3 py-2 rounded hover:bg-[#FF6600] font-semibold ${
            page === pageCount - 1 ? "cursor-not-allowed" : ""
          }`}
        />
      )}
    </div>
  );
};

export default Hero;
