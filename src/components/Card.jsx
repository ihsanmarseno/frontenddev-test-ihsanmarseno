import PropTypes from "prop-types";
import { format } from "date-fns";
import idLocale from "date-fns/locale/id";
import ImageNotFound from "../assets/notfound.png";

const Card = ({ idea }) => {
  const formattedDate = idea.created_at
    ? format(new Date(idea.created_at), "d MMMM yyyy", { locale: idLocale })
    : "";

  return (
    <div className="flex h-full max-w-sm mx-auto mb-4">
      <div className="flex flex-col h-full overflow-hidden bg-white rounded-lg shadow-lg">
        {idea.medium_image && idea.medium_image.length > 0 && (
          <img
            src={idea.medium_image[0].url}
            alt={idea.medium_image[0].file_name}
            className="object-cover w-full h-[12rem]"
            loading="lazy"
          />
        )}
        {!idea.medium_image ||
          (idea.medium_image.length === 0 && (
            <img
              src={ImageNotFound}
              alt="Image Not Found"
              className="w-full h-[12rem] object-contain"
            />
          ))}
        <div className="flex flex-col flex-1 pt-5 pl-4 pr-4">
          {formattedDate && (
            <p className="mt-2 text-sm font-semibold text-gray-400 uppercase">
              {formattedDate}
            </p>
          )}
          <div className="line-clamp-3">
            <h2 className="text-xl font-semibold">{idea.title}</h2>
          </div>
        </div>
      </div>
    </div>
  );
};

Card.propTypes = {
  idea: PropTypes.shape({
    medium_image: PropTypes.arrayOf(
      PropTypes.shape({
        url: PropTypes.string,
        file_name: PropTypes.string,
      })
    ),
    title: PropTypes.string.isRequired,
    date: PropTypes.string,
    created_at: PropTypes.string,
  }).isRequired,
};

export default Card;
