import Banner from './components/Banner'
import Header from './components/Header'
import Hero from './components/Hero'

export default function App() {
  return (
    <div>
      <Header />
      <Banner />
      <Hero />
    </div>
  )
}
